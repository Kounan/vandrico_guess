import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Vandrico_Guesser {
	public static void main(String[] args) throws Exception
	{
		URL guess_url;
		try {
			guess_url = new URL("http://52.8.142.239:8080/guess");
		} catch (MalformedURLException e) {
			// this shouldn't happen since we know this is properly formed
			e.printStackTrace();
			return;
		}
		
		Vandrico_Guesser van_guess = new Vandrico_Guesser(guess_url, 5000);

		int answer = van_guess.get_answer();
		if(answer == -1) {
			System.out.println("Guessing failed.");
		}
		else {
			System.out.println("The answer is " + van_guess.get_answer() + "!");
		}
	}
	
	private URL guess_url;
	private HttpURLConnection guess_connection;
	private int init_guess;
	private int curr_guess;
	private Integer upper_bound;
	private Integer lower_bound;
	private static final String failed = "failed";
	private static final String low = "too low";
	private static final String high = "too high";
	private static final String correct = "correct!";
	
	public Vandrico_Guesser(URL guess_url, int init_guess) {
		this.set_url(guess_url);
		this.set_init_guess(init_guess);
		this.set_curr_guess(init_guess);
		this.set_upper_bound(null);
		this.set_lower_bound(null);
	}
	
	public void set_url(URL guess_url) {
		this.guess_url = guess_url;
	}
	
	public URL get_url() {
		return this.guess_url;
	}
	
	public void set_init_guess(int init_guess) {
		this.init_guess = init_guess;
	}
	
	public int get_init_guess() {
		return this.init_guess;
	}
	
	private void set_curr_guess(int curr_guess) {
		this.curr_guess = curr_guess;
	}
	
	// can't decide if having a getter is useful if I want to make the variable internal
	
	private void set_upper_bound(Integer upper_bound) {
		this.upper_bound = upper_bound;
	}
	
	private void set_lower_bound(Integer lower_bound) {
		this.lower_bound = lower_bound; // hitting a new low
	}
	
	/**
	 * 
	 * @param guess
	 * @param site
	 * @return whether guess is "too low" or "too high"
	 * @throws Exception
	 */
	public String send_guess() throws Exception { // need to learn to handle exceptions better
		
		String utf8 = "UTF-8";
		
		// make the json object
		JSONObject j_guess = new JSONObject();
		j_guess.put("guess", Integer.toString(this.curr_guess));
		OutputStreamWriter toSite = new OutputStreamWriter(this.guess_connection.getOutputStream());
		toSite.write(j_guess.toString());
		toSite.flush(); // send it all
		
		int HttpResult = this.guess_connection.getResponseCode();
		if(HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader resp_msg = new BufferedReader(new InputStreamReader(this.guess_connection.getInputStream(), utf8));
			
			// this can be more robust
			JSONParser json_parser = new JSONParser();
			JSONObject resp_json = (JSONObject) json_parser.parse(resp_msg);
			String high_or_low = (String) resp_json.get("message");			
			resp_msg.close();
			return high_or_low;
		}
		else {
			System.out.println(this.guess_connection.getResponseMessage());
		}
		return "failed";
	}
	
	public int get_answer() throws Exception{
		// TODO: Implement
		// get an answer;
		
		String response;
		do {
			// in terms of performance this connection creation seems to take a long time.
			try {
				this.guess_connection = (HttpURLConnection) this.guess_url.openConnection();
			} catch (IOException e) {
				// not likely to happen in the expected lifetime of this code
				e.printStackTrace();
				return -1;
			}
			
			this.guess_connection.setDoOutput(true);
			this.guess_connection.setRequestMethod("POST");
			this.guess_connection.setRequestProperty("Content-Type", "application/json");
			
			this.guess_connection.connect();
			response = send_guess();
			if(response.equals(Vandrico_Guesser.high)) {
				this.set_upper_bound(this.curr_guess);
				if(this.lower_bound == null) { // haven't hit the floor yet. keep going
					this.set_curr_guess(this.curr_guess / 2);
				}
				else { // previous was low
					this.set_curr_guess((this.curr_guess + this.lower_bound)/2);
				}
			}
			else if(response.equals(Vandrico_Guesser.low)) {
				this.set_lower_bound(this.curr_guess);
				if(this.upper_bound == null) { // haven't hit ceiling. keep going
					this.set_curr_guess(this.curr_guess * 2);
				}
				else { // previous was high
					this.set_curr_guess((this.curr_guess + this.upper_bound)/2);
				}
			}
			else if(response == failed) {
				return -1;
			}
			else {
				return this.curr_guess;
			}
			this.guess_connection.disconnect();
		} while(!response.equals(Vandrico_Guesser.correct) && !response.equals(Vandrico_Guesser.failed));
		return -1;
	}
}
